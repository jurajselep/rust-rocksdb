# build rocksdb locally

## 1.  get rebo adn sub modules
```
git clone https://github.com/rust-rocksdb/rust-rocksdb.git
git submodule update --init --recursive

```

```
git fetch origin pull/446/head:memusage
git checkout memusage
cargo build
cargo build --release

```

# rocks db memory notes
https://github.com/prestodb/presto/issues/8993

https://github.com/facebook/rocksdb/issues/4112


# notes 
```
du -sh ./db/
```

## index for ip:port
find all ids for ip and than get all values for id

ip:port,index ; null

## index for remote;local
find all ids for remote or local

source,index ; null

## index for type
find all ids for type

type, index ; null

## offset, counter 
find id and use range to get counter
id ; value

## combinations

## star combination from end so we can paginate

01 02 10 20 30 ip -> less
10 20 40 50 60 source -> less
02 10 30 80 90 type -> less

