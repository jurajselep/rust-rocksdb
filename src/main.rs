extern crate rocksdb;
extern crate rand;
extern crate num_format;


use rand::Rng; 
use rand::distributions::Alphanumeric;

use num_format::{Locale, ToFormattedString};

use std::time::{SystemTime};

use rocksdb::{Direction, IteratorMode, Options, DB, BlockBasedOptions, Cache, perf::get_memory_usage_stats };

fn get_mem_stats(db : &DB, cache: &Cache) {

    // get mememory stats
    let mem_usage = get_memory_usage_stats(Some(&[&db]), Some(&[&cache])).unwrap();
    println!("\n[+][db][mem_usage] mem_table_total={} ", mem_usage.mem_table_total.to_formatted_string(&Locale::en));
    println!("[+][db][mem_usage] mem_table_unflushed={} ", mem_usage.mem_table_unflushed.to_formatted_string(&Locale::en));
    println!("[+][db][mem_usage] mem_table_readers_total={} ", mem_usage.mem_table_readers_total.to_formatted_string(&Locale::en));
    println!("[+][db][mem_usage] cache_total={}\n", mem_usage.cache_total.to_formatted_string(&Locale::en));

}

fn main() {
    let path = "./db/";
    let log_path = "./log/";

    // set databese options
    let mut db_opts = Options::default();

    db_opts.create_if_missing(true);
    db_opts.create_missing_column_families(true);
    db_opts.set_max_open_files(2);
    db_opts.set_wal_bytes_per_sync(8 << 10); // 8KB
    db_opts.set_writable_file_max_buffer_size(512 << 10); // 512KB
    db_opts.set_enable_write_thread_adaptive_yield(true);
    db_opts.set_unordered_write(false);
    db_opts.set_max_subcompactions(2);
    db_opts.set_max_background_jobs(4);
    db_opts.set_use_adaptive_mutex(true);
    db_opts.set_db_log_dir(&log_path);
    db_opts.set_memtable_whole_key_filtering(true);
    db_opts.set_dump_malloc_stats(true);

    // trigger all sst files in L1/2 instead of L0
    db_opts.set_max_bytes_for_level_base(64 << 10); // 64KB

    // set block based table and cache
    let cache = Cache::new_lru_cache(512 << 10).unwrap();
    let mut block_based_opts = BlockBasedOptions::default();
    block_based_opts.set_block_cache(&cache);
    block_based_opts.set_cache_index_and_filter_blocks(false);
    db_opts.set_block_based_table_factory(&block_based_opts);

    // open db
    let db = DB::open(&db_opts, &path).unwrap();

    // get memory usage
    get_mem_stats(&db, &cache);

    db.put(b"_1", b"test_value").unwrap();
    db.put(b"_2", b" fgfgf ").unwrap();
    db.put(b"_3", b" uiopoiu ").unwrap();
    db.put(b"_4", b" lkwer ").unwrap();
    db.put(b"_5", b" mwnbr ").unwrap();
    db.put(b"_6", b" bnwmr ").unwrap();

    // db.put(b"/1", b" ghjk ").unwrap();
    // db.put(b"/2", b" fgfgf ").unwrap();
    // db.put(b"/3", b" uiopoiu ").unwrap();
    // db.put(b"/4", b" lkwer ").unwrap();
    // db.put(b"/5", b" mwnbr ").unwrap();
    // db.put(b"/6", b" bnwmr ").unwrap();

    // db.put(b"*1", b" ghjk ").unwrap();
    // db.put(b"*2", b" fgfgf ").unwrap();
    // db.put(b"*3", b" uiopoiu ").unwrap();
    // db.put(b"*4", b" lkwer ").unwrap();
    // db.put(b"*5", b" mwnbr ").unwrap();
    // db.put(b"*6", b" bnwmr ").unwrap();

    println!("\n[+][bench][write] start");

    // get current time
    let time_start_write = SystemTime::now();

    // get theard for random number generator
    let rng = rand::thread_rng();

    // create database
    for index in 0..100_000 {

        // let key_prefix = b"10101010";
        
        // create 16 different prefix indexes indexes 
        let key_prefix = i32::from(index % 16).to_be_bytes();
        // use index as counter
        let key_index = i32::from(index).to_be_bytes();
        // concat prefix and index key
        let key = &[key_prefix.clone(), key_index].concat(); 

        // get random value for key
        let value = rng.sample_iter(&Alphanumeric).take(100).collect::<String>();
        
        // print key and value
        // println!("{:?} {:?}", key, value );

        // save to db
        db.put(key, value).unwrap();

    }

    // get current time and display elapsed time
    let elapsed_write = time_start_write.elapsed().unwrap();
    println!("[+][bench][write] time={}ms", elapsed_write.as_millis());

    // get lastest sequence number
    let latest_sequence_number = db.latest_sequence_number();
    println!("[+][db] latest_sequence_number={}", &latest_sequence_number);

    // get current time
    let time_start_get = SystemTime::now();
    
    // get value for key
    match db.get(vec![0, 0, 0, 15, 0, 16, 67, 223]) {
        Ok(Some(value)) => println!("[+][db] retrieved value: {}", String::from_utf8(value).unwrap()),
        Ok(None) => println!("[-][db] value not found !"),
        Err(e) => println!("[-][error] operational problem encountered: {}", e),
    }

    // get current time and display elapsed time
    let elapsed_get = time_start_get.elapsed().unwrap();
    println!("\n[+][bench][get] time={}μs", elapsed_get.as_micros());
 
    // get memory usage
    get_mem_stats(&db, &cache);

    // always iterates forward
    // get current time
    let time_start_read = SystemTime::now();
    let mut counter_read: i64 = 0;

    {
        // always iterates forward
        // let iter = db.prefix_iterator(vec![0, 0, 0, 6]);
        let iter = db.iterator(IteratorMode::End);
        //let iter = db.iterator(IteratorMode::Start);
        for (_key, _value) in iter {
            // counter
            counter_read = counter_read + 1;
            // println!("[iter] start: {:?} {:?}", String::from_utf8(key.into_vec()).unwrap(), String::from_utf8(value.into_vec()).unwrap() );
            // println!("[iter] start: {:?} {:?}", key, String::from_utf8(value.into_vec()).unwrap() );
        }
        
        println!("[+][bench][read] counter={}", counter_read);
    }
    
    // get current time and display elapsed time
    let elapsed_read = time_start_read.elapsed().unwrap();
    println!("[+][bench][read] time={}ms", elapsed_read.as_millis());

    
    // get memory usage
    get_mem_stats(&db, &cache);

    // get value for key
    match db.get(b"_1") {
        Ok(Some(value)) => println!("[+][db] key=_1  value={}", String::from_utf8(value).unwrap()),
        Ok(None) => println!("[-][db] value not found !"),
        Err(e) => println!("[-][error] operational problem encountered: {}", e),
    }

    // trigger flush so we check cache
    db.flush().unwrap();

    // get memory usage
    get_mem_stats(&db, &cache);

    // always iterates backward
    let iter = db.iterator(IteratorMode::End);
    for (key, value) in iter {
        counter_read = counter_read + 1;
        // println!("[iter] end: {:?} {:?}", String::from_utf8(key.into_vec()).unwrap(),  String::from_utf8(value.into_vec()).unwrap() );
    }

    // from a key in Direction::{forward,reverse}
    let iter = db.iterator(IteratorMode::From(b"_3", Direction::Forward));
    for (key, value) in iter {
        counter_read = counter_read + 1;
        // println!("[iter] from(forw): {:?} {:?}", String::from_utf8(key.into_vec()).unwrap(),  String::from_utf8(value.into_vec()).unwrap() );
    }

    // you can seek with an existing Iterator instance, too
    let iter = db.iterator(IteratorMode::Start);
    // let iter.set_mode(IteratorMode::From(b"_3", Direction::Reverse));
    for (key, value) in iter {
        counter_read = counter_read + 1;
        // println!("[iter] from(rev): {:?} {:?}", String::from_utf8(key.into_vec()).unwrap(),  String::from_utf8(value.into_vec()).unwrap() );
    }

    println!("[+][bench][read] counter={}", counter_read);

    // get memory usage
    get_mem_stats(&db, &cache);


    // db.delete(b"my key").unwrap();
    let _ = DB::destroy(&Options::default(), path);
}
